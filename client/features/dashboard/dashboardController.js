'use strict';

const request = require('request');
const path = require('path');
const config = require('../../../server/config/config');

const dashboard = {
    // route to show the page for "/"
    index: (req, res) => {
        res.render(path.join(__dirname + '/views/index.ejs'));
    },

    // route to fetch data from "/api/adx" and render page for "/adx"
    adx: (req, res) => {
        request(config.apiPath + '/api/adx', (error, response, body) => {
            if(error) {
                res.send("Error: " + error);
            };

            res.render(path.join(__dirname + '/views/adx/adx.ejs'), {
                results: JSON.parse(body),
                reportPath: config.reportPath.adx
            });
        });
    },

    // route to fetch data from "/api/adx/reponse" and render page for "/adx/response/:complaintno"
    adxComplaintResponse: (req, res) => {
        request(config.apiPath + '/api/adx/response/' + req.params.complaintno, (error, response, body) => {
            if(error) {
                res.send("Error: " + error);
            };

            res.render(path.join(__dirname + '/views/adx/adxComplaintResponse.ejs'), {
                complaint: JSON.parse(body)
            });
        });
    },

    // route to fetch data from "/api/adx/solution" and render page for "/adx/solution/:complaintno"
    adxComplaintSolution: (req, res) => {
        request(config.apiPath + '/api/adx/solution/' + req.params.complaintno, (error, response, body) => {
            if(error) {
                res.send("Error: " + error);
            };

            res.render(path.join(__dirname + '/views/adx/adxComplaintSolution.ejs'), {
                complaint: JSON.parse(body)
            });
        });
    },

    // route to fetch data from "/api/adx/followup" and render page for "/adx/followup/:complaintno"
    adxComplaintFollowup: (req, res) => {
        request(config.apiPath + '/api/adx/followup/' + req.params.complaintno, (error, response, body) => {
            if(error) {
                res.send("Error: " + error);
            };

            res.render(path.join(__dirname + '/views/adx/adxComplaintFollowup.ejs'), {
                complaint: JSON.parse(body)
            });
        });
    },

    // route to fetch data from "/api/adx/note/update/" and render page for "/adx/note/update/:complaintno"
    adxNoteUpdate: (req, res) => {
        request(config.apiPath + '/api/adx/note/update/' + req.params.complaintno, (error, response, body) => {
            if(error) {
                res.send("Error: " + error);
            };

            res.render(path.join(__dirname + '/views/adx/adxNoteUpdate.ejs'), {
                complaint: JSON.parse(body)
            });
        });
    },

    // route to fetch data from "/api/adx/note/edit/" and render page for "/adx/note/edit/:complaintno"
    adxNoteEdit: (req, res) => {
        request(config.apiPath + '/api/adx/note/edit/' + req.params.complaintno, (error, response, body) => {
            if(error) {
                res.send("Error: " + error);
            };

            res.render(path.join(__dirname + '/views/adx/adxNoteEdit.ejs'), {
                complaint: JSON.parse(body)
            });
        });
    },

    // route to fetch data from "/api/adx/urgentnote/edit" and render page for "/adx/urgentnote/edit/:complaintno"
    adxUrgentNoteEdit: (req, res) => {
        request(config.apiPath + '/api/adx/urgentnote/edit/' + req.params.complaintno, (error, response, body) => {
            if(error) {
                res.send("Error: " + error);
            };

            res.render(path.join(__dirname + '/views/adx/adxUrgentNoteEdit.ejs'), {
                complaint: JSON.parse(body)
            });
        });
    },

    // route to fetch data from "/api/adx" and render page for "/aa"
    aa: (req, res) => {
        request(config.apiPath + '/api/aa', (error, response, body) => {
            if(error) {
                res.send("Error: " + error);
            };

            res.render(path.join(__dirname + '/views/aa/aa.ejs'), {
                results: JSON.parse(body),
                reportPath: config.reportPath.aa
            });
        });
    },

    // route to fetch data from "/api/aa/reponse" and render page for "/aa/response/:complaintno"
    aaComplaintResponse: (req, res) => {
        request(config.apiPath + '/api/aa/response/' + req.params.complaintno, (error, response, body) => {
            if(error) {
                res.send("Error: " + error);
            };

            res.render(path.join(__dirname + '/views/aa/aaComplaintResponse.ejs'), {
                complaint: JSON.parse(body)
            });
        });
    },

    // route to fetch data from "/api/aa/solution" and render page for "/aa/solution/:complaintno"
    aaComplaintSolution: (req, res) => {
        request(config.apiPath + '/api/aa/solution/' + req.params.complaintno, (error, response, body) => {
            if(error) {
                res.send("Error: " + error);
            };

            res.render(path.join(__dirname + '/views/aa/aaComplaintSolution.ejs'), {
                complaint: JSON.parse(body)
            });
        });
    },

    // route to fetch data from "/api/aa/followup" and render page for "/aa/followup/:complaintno"
    aaComplaintFollowup: (req, res) => {
        request(config.apiPath + '/api/aa/followup/' + req.params.complaintno, (error, response, body) => {
            if(error) {
                res.send("Error: " + error);
            };

            res.render(path.join(__dirname + '/views/aa/aaComplaintFollowup.ejs'), {
                complaint: JSON.parse(body)
            });
        });
    },

    // route to fetch data from "/api/aa/note/update/" and render page for "/aa/note/update/:complaintno"
    aaNoteUpdate: (req, res) => {
        request(config.apiPath + '/api/aa/note/update/' + req.params.complaintno, (error, response, body) => {
            if(error) {
                res.send("Error: " + error);
            };

            res.render(path.join(__dirname + '/views/aa/aaNoteUpdate.ejs'), {
                complaint: JSON.parse(body)
            });
        });
    },

    // route to fetch data from "/api/aa/note/edit/" and render page for "/aa/note/edit/:complaintno"
    aaNoteEdit: (req, res) => {
        request(config.apiPath + '/api/aa/note/edit/' + req.params.complaintno, (error, response, body) => {
            if(error) {
                res.send("Error: " + error);
            };

            res.render(path.join(__dirname + '/views/aa/aaNoteEdit.ejs'), {
                complaint: JSON.parse(body)
            });
        });
    },

    // route to fetch data from "/api/aa/urgentnote/edit" and render page for "/aa/urgentnote/edit/:complaintno"
    aaUrgentNoteEdit: (req, res) => {
        request(config.apiPath + '/api/aa/urgentnote/edit/' + req.params.complaintno, (error, response, body) => {
            if(error) {
                res.send("Error: " + error);
            };

            res.render(path.join(__dirname + '/views/aa/aaUrgentNoteEdit.ejs'), {
                complaint: JSON.parse(body)
            });
        });
    },
};

module.exports = dashboard;
