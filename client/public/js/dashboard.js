"use strict";

$(function() {
    // Allows user to change urgency of a complaint without refreshing the page
    $('form#urgentForm').submit(function(e) {
        e.preventDefault();

        var url = e.currentTarget.action;
        var urgent = e.currentTarget.children[1].value;
        var changeUrgent = e.currentTarget.children[1];
        var urgentValue = $(e.currentTarget).siblings();
        var row = $(e.currentTarget).parents();
        var noteButton = $(e.currentTarget.children[2]);

        $.ajax({
            url: url,
            type: 'post',
            contentType: 'application/json',
            data: JSON.stringify({ urgent: urgent })
        }).done(function(data) {
            if(data == '0') {
                $(urgentValue[0]).html('No');
                $(changeUrgent).val('No').change();
                $(noteButton).css({'visibility': 'hidden'});
                $(row[0]).css({'background': 'white', 'color': 'black'});
            } else {
                $(urgentValue[0]).html('Yes');
                $(changeUrgent).val('Yes').change();
                $(noteButton).css({'visibility': 'visible'});
                $(row[0]).css({'background': 'orangered', 'color': 'white'});
            };
        });
    });

    // Allows user to change death/injury of a complaint without refreshing the page
    $('form#deathInjuryForm').submit(function(e) {
        e.preventDefault();

        var url = e.currentTarget.action;
        var deathInjury = e.currentTarget.children[1].value;
        var changeDeathInjury = e.currentTarget.children[1];
        var deathInjuryValue = $(e.currentTarget).siblings();
        var row = $(e.currentTarget).parents()

        $.ajax({
            url: url,
            type: 'post',
            contentType: 'application/json',
            data: JSON.stringify({ deathInjury: deathInjury })
        }).done(function(data) {
            if(data == '0') {
                $(deathInjuryValue[0]).html('No');
                $(changeDeathInjury).val('No').change();
                $(row[0]).css({'background': 'white', 'color': 'black'});
            } else {
                $(deathInjuryValue[0]).html('Yes');
                $(changeDeathInjury).val('Yes').change();
                $(row[0]).css({'background': 'red', 'color': 'white'});
            };
        });
    });

    // Datepicker for followup section of the dashboard
    $('#followupDate').datepicker();

    // Remembers scrollbar location after refresh
    $("tbody").scroll(function() {
        sessionStorage.scrollTop = $(this).scrollTop();
    });

    $(document).ready(function() {
        if (sessionStorage.scrollTop != "undefined") {
            $("tbody").scrollTop(sessionStorage.scrollTop);
        }
    });

    // Prompt pop-up to comfirm if you want to close the complaint for AA
    $('form#closeComplaint').submit(function(e) {
        e.preventDefault();

        var answer = prompt("Do you want to close this complaint? Yes / No");

        if (answer === "Yes" || answer === "yes" || answer === "y" || answer === "Y") {
            $(this).unbind('submit').submit();
        };
    });

    // Allows user to click the "Path" button and have the folder path copied to clipboard
    new Clipboard(".reportPath");

    // Stylizes the table and adds more features to it
    $.extend($.tablesorter.themes.default, {
        table: 'ui-widget ui-widget-content ui-corner-all',
        caption: 'ui-widget-content',
        header: 'ui-widget-header ui-corner-all ui-state-default',
        sortNone: '',
        sortAsc: '',
        sortDesc: '',
        icons: 'ui-icon',
        iconSortNone: 'ui-icon-carat-2-n-s ui-icon-caret-2-n-s',
        iconSortAsc: 'ui-icon-carat-1-n ui-icon-caret-1-n',
        iconSortDesc: 'ui-icon-carat-1-s ui-icon-caret-1-s',
    });

    // Defaults table sort from newest to oldersts complaints
    var creation = $('th:contains("Creation")').index();

    // Allows certain columns on table to be sorted
    $("#table").tablesorter({
        theme: 'default',
        sortList: [[ creation, 1 ]],
        headerTemplate: '{content} {icon}',
    	headers: {
    	    1: {
                // sorter: false,
                filter: false
    	    },

    	    3: {
                // sorter: false,
                filter: false
    	    },

    	    4: {
                // sorter: false
    	    },

    	    5: {
                // sorter: false
    	    },

    	    6: {
                // sorter: false
    	    },

    	    7: {
                // sorter: false
    	    },

    	    8: {
                // sorter: false,
                filter: false
    	    },

    	    9: {
                // sorter: false
    	    },

    	    10: {
                // sorter: false
    	    },

    	    11: {
                // sorter: false
    	    },

            12: {
                // sorter: false
    	    },

            13: {
                sorter: false
    	    }
    	}
    });
});
