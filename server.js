'use strict';

const express = require('express');
const app = express();
const moment = require('moment');
const _ = require('lodash');
const bodyParser = require('body-parser');
const compression = require('compression');
const expressValidator = require('express-validator');
const config = require('./server/config/config');
const adx = require('./server/api/adxController');
const aa = require('./server/api/aaController');
const dashboard = require('./client/features/dashboard/dashboardController');

// set functions for the templating engine to use
app.locals.moment = moment;
app.locals.htmlDisplay = html => _.escape(html).replace(/\n/g, '<br>');
app.locals.rmWhitespace = true;

// middleware that the app uses
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressValidator());
app.use('/static', express.static('./client/public'));

// set the templating engine
app.set('view engine', 'ejs');

// routes to get and post data to adx
app.get('/api/adx', adx.adx);
app.get('/api/adx/response/:complaintno', adx.complaintResponse);
app.post('/api/adx/response/', adx.complaintResponseUpdate);
app.get('/api/adx/solution/:complaintno', adx.complaintSolution);
app.post('/api/adx/solution/', adx.complaintSolutionUpdate);
app.get('/api/adx/followup/:complaintno', adx.complaintFollowup);
app.post('/api/adx/followup/', adx.complaintFollowupUpdate);
app.post('/api/adx/clearfollowup', adx.clearFollowup);
app.get('/api/adx/note/update/:complaintno', adx.updateNote);
app.post('/api/adx/note/update', adx.noteUpdate);
app.get('/api/adx/note/edit/:complaintno', adx.editNote);
app.post('/api/adx/note/edit', adx.noteEdit);
app.post('/api/adx/urgent/:complaintno', adx.urgent);
app.post('/api/adx/death_injury/:complaintno', adx.deathInjury);
app.get('/api/adx/urgentnote/edit/:complaintno', adx.editUrgentNote);
app.post('/api/adx/urgentnote/edit', adx.urgentNoteEdit);

// routes to get and post data to aa
app.get('/api/aa', aa.aa);
app.get('/api/aa/response/:complaintno', aa.complaintResponse);
app.post('/api/aa/response/', aa.complaintResponseUpdate);
app.get('/api/aa/solution/:complaintno', aa.complaintSolution);
app.post('/api/aa/solution/', aa.complaintSolutionUpdate);
app.get('/api/aa/followup/:complaintno', aa.complaintFollowup);
app.post('/api/aa/followup/', aa.complaintFollowupUpdate);
app.get('/api/aa/note/update/:complaintno', aa.updateNote);
app.post('/api/aa/note/update', aa.noteUpdate);
app.get('/api/aa/note/edit/:complaintno', aa.editNote);
app.post('/api/aa/note/edit', aa.noteEdit);
app.post('/api/aa/urgent/:complaintno', aa.urgent);
app.post('/api/aa/status/:complaintno', aa.status);
app.post('/api/aa/death_injury/:complaintno', aa.deathInjury);
app.get('/api/aa/urgentnote/edit/:complaintno', aa.editUrgentNote);
app.post('/api/aa/urgentnote/edit', aa.urgentNoteEdit);

// home page route
app.get('/', dashboard.index);

// adx routes for the dashboard
app.get('/adx', dashboard.adx);
app.get('/adx/response/:complaintno', dashboard.adxComplaintResponse);
app.get('/adx/solution/:complaintno', dashboard.adxComplaintSolution);
app.get('/adx/followup/:complaintno', dashboard.adxComplaintFollowup);
app.get('/adx/note/update/:complaintno', dashboard.adxNoteUpdate);
app.get('/adx/note/edit/:complaintno', dashboard.adxNoteEdit);
app.get('/adx/urgentnote/edit/:complaintno', dashboard.adxUrgentNoteEdit);

// aa routes for the dashboard
app.get('/aa', dashboard.aa);
app.get('/aa/response/:complaintno', dashboard.aaComplaintResponse);
app.get('/aa/solution/:complaintno', dashboard.aaComplaintSolution);
app.get('/aa/followup/:complaintno', dashboard.aaComplaintFollowup);
app.get('/aa/note/update/:complaintno', dashboard.aaNoteUpdate);
app.get('/aa/note/edit/:complaintno', dashboard.aaNoteEdit);
app.get('/aa/urgentnote/edit/:complaintno', dashboard.aaUrgentNoteEdit);

// runs the server
app.listen(config.port, config.ip);

module.exports = app;
