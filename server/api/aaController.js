// logic and queries for AA portion of the dashboard
'use strict';

const ADODB = require('node-adodb');
const moment = require('moment');
const config = require('../config/config');
const connection = ADODB.open(config.source.aa);

// UNCOMMENT TO SEE QUERY IN CONSOLE
ADODB.debug = true;

const aa = {
    // gets all the "open" complaints
    aa: (req, res) => {
        connection
            .query("SELECT c.ComplaintNo, c.Update_date, c.Subject, c.InvoiceNo, c.CustCode, c.RGA_NO, r.RGA_STATUS, r.RGACode, c.Warranty_OrderNo, w.PO, w.Warr_Invno, w.Warr_inv_date, c.response_date, c.solution_date, o.TrackNo, c.Notes, re.Receipt_date, c.response_initial, c.solution_initial, c.urgent, c.death_injury, c.urgent_note, c.followup_date, c.followup_note FROM (((cpl_link AS c LEFT JOIN rga_header AS r ON c.RGA_NO = r.RGA_NO) LEFT JOIN warranty_header AS w ON c.Warranty_ID = w.Warranty_ID) LEFT JOIN OrdersWithTrackingNo AS o ON c.Warranty_OrderNo = o.Ordno) LEFT JOIN RGA_Receipts AS re ON c.RGA_NO = re.RGA_NO WHERE c.Compl_Status = 'OPEN'")
            .on('done', (result) => {
                res.json(result.records, null, 2);
            })
            .on('fail', (error) => {
                res.json({
                    "Error": error
                });
            })
    },

    // sets the status of the complaint to closed and the date to current date/time
    status: (req, res) => {
        let complaintNo = req.params.complaintno;
        let status = "CLOSED";
        let closedDate = moment(Date.now()).format('M/D/YYYY h:mm:ss A');

        connection
            .execute("UPDATE cpl_link SET Date_Closed = '" + closedDate + "', Compl_Status = '" + status + "' WHERE ComplaintNo = " + complaintNo)
            .on('done', (result) => {
                res.redirect('/aa');
            })
            .on('fail', (error) => {
                res.json({
                    "Error": error
                });
            })
    },

    // toggles death/injury from "true" to "false" or vice versa
    deathInjury: (req, res) => {
        let complaintNo = req.params.complaintno;
        let deathInjury = -1;

        if(req.body.deathInjury == 'Yes'){
            deathInjury = 0;
        };

        connection
            .execute("UPDATE cpl_link SET death_injury = '" + deathInjury + "' WHERE complaintno = " + complaintNo)
            .on('done', (result) => {
                res.json(deathInjury);
            })
            .on('fail', (error) => {
                res.json({
                    "Error": error
                });
            })
    },

    // toggles urgent from "true" to "false" or vice versa
    urgent: (req, res) => {
        let complaintNo = req.params.complaintno;
        let urgent = -1;

        if(req.body.urgent == 'Yes') {
            urgent = 0;
        };

        connection
            .execute("UPDATE cpl_link SET urgent = '" + urgent + "' WHERE ComplaintNo = " + complaintNo)
            .on('done', (result) => {
                // res.redirect('/aa');
                res.json(urgent);
            })
            .on('fail', (error) => {
                res.json({
                    "Error": error
                });
            })
    },

    // gets the specific complaint that needs to be updated
    updateNote: (req, res) => {
        let complaintNo = req.params.complaintno;

        connection
            .query("SELECT ComplaintNo, Notes FROM cpl_link WHERE ComplaintNo = " + complaintNo)
            .on('done', (result) => {
                res.json(result.records, null, 2);
            })
            .on('fail', (error) => {
                res.json({
                    "Error": error
                });
            })
    },

    // appends the new update to the existing note and time stamps it before putting it into the database
    noteUpdate: (req, res) => {
        let response_date = "(" + moment(Date.now()).format('M/D/YYYY') + ")";
        let complaintNo = parseInt(req.body.complaintno);
        let chrcode = "' & Chr(13) & Chr(10) &'";
        let note = req.body.note.replace(/'/g, "''");
        let update = req.body.update.replace(/'/g, "''");
        let newNote = note + chrcode + response_date + chrcode + update + chrcode;

        req.checkBody('update', 'Update can not be blank').notEmpty();

        let errors = req.validationErrors();

        if(errors) {
            res.status(400).send('Error: ' + errors[0].msg);
            return;
        };

        connection
            .execute("UPDATE cpl_link SET notes = '" + newNote + "' WHERE ComplaintNo = " + complaintNo)
            .on('done', (result) => {
                res.redirect('/aa');
            })
            .on('fail', (error) => {
                res.json({
                    "Error": error
                });
            })
    },

    // gets the specific urgent note that needs to be edited
    editUrgentNote: (req, res) => {
        let complaintNo = req.params.complaintno;

        connection
            .query("SELECT ComplaintNo, urgent_note FROM cpl_link WHERE ComplaintNo = " + complaintNo)
            .on('done', (result) => {
                res.json(result.records, null, 2);
            })
            .on('fail', (error) => {
                res.json({
                    "Error": error
                });
            })
    },

    // sends the edited urgent note to the complaint
    urgentNoteEdit: (req, res) => {
        let note = req.body.note;
        let complaintNo = req.body.complaintno;
        let editedNote = note.replace(/'/g, "''");

        connection
            .execute("UPDATE cpl_link SET urgent_note = '" + editedNote + "' WHERE ComplaintNo = " + complaintNo)
            .on('done', (result) => {
                res.redirect('/aa');
            })
            .on('fail', (error) => {
                res.json({
                    "Error": error
                });
            })
    },

    // gets the specific note that needs to be edited
    editNote: (req, res) => {
        let complaintNo = req.params.complaintno;

        connection
            .query("SELECT ComplaintNo, Notes FROM cpl_link WHERE ComplaintNo = " + complaintNo)
            .on('done', (result) => {
                res.json(result.records, null, 2);
            })
            .on('fail', (error) => {
                res.json({
                    "Error": error
                });
            })
    },

    // sends the edited note to the complaint
    noteEdit: (req, res) => {
        let note = req.body.note;
        let complaintNo = req.body.complaintno;
        let editedNote = note.replace(/'/g, "''");

        connection
            .execute("UPDATE cpl_link SET notes = '" + editedNote + "' WHERE ComplaintNo = " + complaintNo)
            .on('done', (result) => {
                res.redirect('/aa');
            })
            .on('fail', (error) => {
                res.json({
                    "Error": error
                });
            })
    },

    // gets the specific complaint that needs 1st response logged
    complaintResponse: (req, res) => {
        let complaintNo = req.params.complaintno;

        connection
            .query("SELECT ComplaintNo FROM cpl_link WHERE ComplaintNo = " + complaintNo)
            .on('done', (result) => {
                res.json(result.records, null, 2);
            })
            .on('fail', (error) => {
                res.json({
                    "Error": error
                });
            })
    },

    // gets the specific complaint that needs the solution logged
    complaintSolution: (req, res) => {
        let complaintNo = req.params.complaintno;

        connection
            .query("SELECT ComplaintNo FROM cpl_link WHERE ComplaintNo = " + complaintNo)
            .on('done', (result) => {
                res.json(result.records, null, 2);
            })
            .on('fail', (error) => {
                res.json({
                    "Error": error
                });
            })
    },

    // gets the specific complaint that needs the solution logged
    complaintFollowup: (req, res) => {
        let complaintNo = req.params.complaintno;

        connection
            .query("SELECT ComplaintNo, followup_date, followup_note FROM cpl_link WHERE ComplaintNo = " + complaintNo)
            .on('done', (result) => {
                res.json(result.records, null, 2);
            })
            .on('fail', (error) => {
                res.json({
                    "Error": error
                });
            })
    },

    // sends the inputted initial for 1st reponse and date/time to the database
    complaintResponseUpdate: (req, res) => {
        req.checkBody('responseInitial', 'Initials can not be blank').notEmpty();

        let errors = req.validationErrors();

        if(errors) {
            res.status(400).send('Error: ' + errors[0].msg);
            return;
        };

        let response_date = moment(Date.now()).format('M/D/YYYY h:mm:ss a');
        let response_initial = req.body.responseInitial.toUpperCase();
        let complaintNo = req.body.complaintno;

        connection
            .execute("UPDATE cpl_link SET response_date = '" + response_date + "', response_initial = '" + response_initial + "' WHERE ComplaintNo = " + complaintNo)
            .on('done', (result) => {
                res.redirect('/aa');
            })
            .on('fail', (error) => {
                res.json({
                    "Error": error
                });
            })
    },

    // sends the inputted initial for solution and date/time to the database
    complaintSolutionUpdate: (req, res) => {
        req.checkBody('solutionInitial', 'Initials can not be blank').notEmpty();

        let errors = req.validationErrors();

        if(errors) {
            res.status(400).send('Error: ' + errors[0].msg);
            return;
        };

        let solutionDate = moment(Date.now()).format('M/D/YYYY h:mm:ss a');
        let solutionInitial = req.body.solutionInitial.toUpperCase();
        let complaintNo = req.body.complaintno;

        connection
            .execute("UPDATE cpl_link SET solution_date = '" + solutionDate + "', solution_initial = '" + solutionInitial + "' WHERE ComplaintNo = " + complaintNo)
            .on('done', (result) => {
                res.redirect('/aa');
            })
            .on('fail', (error) => {
                res.json({
                    "Error": error
                });
            })
    },

    // sends the form information for follow up to the database
    complaintFollowupUpdate: (req, res) => {
        let complaintNo = req.body.complaintno;
        let followupDate = req.body.followupDate;
        let followupNote = req.body.note.replace(/'/g, "''");;

        connection
            .execute("UPDATE cpl_link SET followup_date = '" + followupDate + "', followup_note = '" + followupNote + "' WHERE ComplaintNo = " + complaintNo)
            .on('done', (result) => {
                res.redirect('/aa');
            })
            .on('fail', (error) => {
                res.json({
                    "Error": error
                });
            })
    },
};

module.exports = aa;
