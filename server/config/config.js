'use strict';

const config = {
    port: 3000,
    ip: '0.0.0.0',
    apiPath: 'http://localhost:3000',
    reportPath: {
        adx: 'file://accdata/AMICODIAGUSERS/ADX/Customer%20Service/ADX-Complaints',
        aa: 'file://accdata/AmicoAccUsers/13%20Customer%20Service/AAComplaints'
    },
    source: {
        adx: "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=//accdata/AMICODIAGUSERS/AmicoDiag/Programs/DBFiles/QualityReport.mdb;Persist Security Info=False;",
        aa: "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=//accdata//ITFolder/NON CONFORMANCE/QC Non conformance/QualityReport.mdb;Persist Security Info=False;"
    }
    // source: {
    //     adx: "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=//accdata/AMICODIAGUSERS/AmicoDiag/Programs/DBFiles/QualityReport_test.mdb;Persist Security Info=False;",
    //     aa: "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=//accdata//ITFolder/NON CONFORMANCE/QC Non conformance/QualityReport_test.mdb;Persist Security Info=False;"
    // }
};

module.exports = config;
