'use strict';

const gulp = require('gulp');
const inject = require('gulp-inject');

gulp.task('inject', () => {
    let target = gulp.src('client/public/partials/header.ejs')
    let sources = gulp.src([
        'client/public/js/jquery-3.2.0.min.js',
        'client/public/js/jquery.tablesorter.min.js',
        'client/public/js/*.js',
        'client/public/css/*.css'
    ], {
         read: false
    });

    return target
        .pipe(inject(sources, {
            ignorePath: 'client/public',
            addPrefix: '/static'
        }))
        .pipe(gulp.dest('client/public/partials'))
});

gulp.task('default', ['inject']);
